<?php
include_once $_SERVER["DOCUMENT_ROOT"].'/includes/functions/functions.php';
include_once $_SERVER["DOCUMENT_ROOT"].'/includes/functions/contact-functions.php';
include_once $_SERVER["DOCUMENT_ROOT"].'/includes/functions/data/connecteur.php';
include_once $_SERVER["DOCUMENT_ROOT"].'/includes/functions/data/contact.php';
include_once $_SERVER["DOCUMENT_ROOT"].'/includes/parts/header.php';
include_once $_SERVER["DOCUMENT_ROOT"].'/includes/search-header.php';
?>

<section>
    <h1 class="title is-1">Contactez-nous!</h1>
    <div class="container is-fluid">
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
            <input type="hidden" name="contact">
            <input placeholder="nom" type="text" name="nom" value="<?php form_values("nom") ?>"><br>
            <?php 
                echo (isset($errors["nom"]) ? "<div class='error'>". $errors["nom"]."</div>":"");
            ?>
            <input placeholder="prenom" type="text" name="prenom" value="<?php form_values("prenom") ?>"><br>
            <?php 
                echo (isset($errors["prenom"]) ? "<div class='error'>". $errors["prenom"]."</div>":""); 
            ?>
            <input placeholder="courriel" type="email" name="courriel"  value="<?php form_values("courriel") ?>"><br>
            <?php 
                echo (isset($errors["courriel"]) ? "<div class='error'>". $errors["courriel"]."</div>":""); 
            ?>
            <textarea name="contact" placeholder="votre message" id="contact" cols="30" rows="10"><?php form_values("contact") ?></textarea><br>
            <?php
                echo (isset($errors["contact"]) ? "<div class='error'>". $errors["contact"]."</div>":""); 
            ?>
            <input type="submit" value="Soumettre votre demande">
        </form>
    </div>
</section>

<?php
include_once './includes/parts/comment-list.php';
include_once './includes/parts/footer.php';