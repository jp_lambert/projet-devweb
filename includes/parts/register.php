<section>
    <h1 class="title is-1">Devenir membre!</h1>
    <div class="container is-fluid">
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
            <input type="hidden" name="register">
            <input placeholder="nom d'utilisateur" type="text" name="username" value="<?php form_values("username") ?>"><br>
            <?php 
                echo (isset($errors["username"]) ? "<div class='error'>". $errors["username"]."</div>":"");
            ?>
            <input placeholder="courriel" type="email" name="courriel"  value="<?php form_values("courriel") ?>"><br>
            <?php 
                echo (isset($errors["courriel"]) ? "<div class='error'>". $errors["courriel"]."</div>":""); 
            ?>
            <input placeholder="motdepasse" type="password" name="pass"  value="<?php form_values("pass") ?>"><br>
            <?php 
                echo (isset($errors["pass"]) ? "<div class='error'>". $errors["pass"]."</div>":""); 
            ?>
            <input type="submit" value="Vous enregistrer">
        </form>
    </div>
</section>