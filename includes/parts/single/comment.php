<?php 
    include_once $_SERVER["DOCUMENT_ROOT"].'/includes/functions/functions.php';
    include_once folder('/includes/parts/header.php');
    include_once folder('/includes/search-header.php');
    include_once folder('/includes/functions/data/contact.php');

    $comment_by_id = comment_by_id($_GET["id"]);
    $comment = $comment_by_id->fetch();
?>

<h1>Commentaire</h1>
<div><?php echo $comment["nom"] ?> <?php echo $comment["prenom"] ?> a dit:</div>
<div><?php echo $comment["contact"] ?></div>
<?php
    include_once folder('/includes/parts/footer.php');
?>