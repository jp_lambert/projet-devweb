<section>
    <h1 class="title is-1">Login</h1>
    <div class="container is-fluid">
        <?php echo (isset($_GET["message"])? $_GET["message"]: ""); ?>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
            <input type="hidden" name="login">
            <input placeholder="nom d'utilisateur" type="text" name="username" value="<?php form_values("username") ?>"><br>
            <?php 
                echo (isset($errors["courriel"]) ? "<div class='error'>". $errors["courriel"]."</div>":""); 
            ?>
            <input placeholder="motdepasse" type="password" name="pass"  value="<?php form_values("pass") ?>"><br>
            <?php 
                echo (isset($errors["pass"]) ? "<div class='error'>". $errors["pass"]."</div>":""); 
            ?>
            <input type="submit" value="Soumettre">
        </form>
    </div>
</section>