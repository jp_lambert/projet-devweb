<?php 
include_once $_SERVER['DOCUMENT_ROOT'].'/includes/functions/data/connecteur.php';

function all_comments(){
    $conn = connect();
    try{
        $requete = $conn->query('SELECT * from comments');
        return $requete; 
    } catch(PDOException $e){
        echo 'Erreur : '.$e->getMessage();
    }
    $conn = null;
    exit();
}

function comment_by_id($id){
    $conn = connect();
    try{
        $requete = $conn->prepare("SELECT * FROM comments WHERE Id = :id");
        $requete->execute([":id"=>$id]);
        return $requete; 
    } catch(PDOException $e){
        echo 'Erreur : '.$e->getMessage();
    }
    $conn = null;
    exit();
}
