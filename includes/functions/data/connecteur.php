<?php 
include_once $_SERVER["DOCUMENT_ROOT"].'/includes/functions/config.php';

function connect(){
    $serveur = DB_SERVER; //127.0.0.1
    $dbname = DB_DATABASE; 
    $user = DB_USERNAME;
    $password = DB_PASSWORD;
    $options = [
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, 
        PDO::ATTR_EMULATE_PREPARES => false, 
    ];

    try{
        $db_connection = new PDO("mysql:host=$serveur;dbname=$dbname", $user, $password, $options);
        return $db_connection;
    } catch ( PDOException $e ) {
        echo $e->getMessage();
    }
}