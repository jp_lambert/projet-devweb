<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/includes/functions/data/connecteur.php';

/**
 * Register user
 *
 * @param [array] $user_data
 * @return void
 */
function register_user($user_data)
{   
    $db = connect();
    //Les seules methodes d'encryption acceptes sont Bcrypt, sha256, or sha512
    $exists = get_user_by_email($user_data)->fetchAll();
    if (count($exists) < 1)
    {
        $encpwd = hash("sha512", $user_data["pass"]);
        try{
            $register = $db->prepare("
                INSERT INTO users(username, password, email) VALUES
                (:username, :pass, :email);
            ");
            $register->execute([
                ":username" => $user_data["username"],
                ":email" => $user_data["courriel"],
                ":pass" => $encpwd
            ]);
            $_SESSION["user_id"] = $db->lastInsertId();
            return "Votre utilisateur a bien été enregistré";
        } catch (PDOException $e){
            echo $e->getMessage();
        }
    }
}

/**
 * Retrieve user with email
 *
 * @param [type] $user_data
 * @return void
 */
function get_user_by_email($user_data)
{
    $db = connect();
    try{
        $query = $db->prepare("SELECT * FROM users WHERE email=:email");
        $query->execute([":email" => $user_data['courriel']]);
        return $query;
    } catch (PDOException $e) {
        echo $e->getMessage();
        exit();
    }
}

function login($login_data)
{
    $db = connect();
    $password = $login_data["pass"];
    $hashed= hash('sha512', $password); //Password encryption 

    try{
        $query= $db->prepare("SELECT id FROM users WHERE email=:email AND password=:hashed");
        $query->execute([
            ":email" => $login_data["username"],
            ":hashed" => $hashed
        ]);
        $data=$query->fetch();
    } catch (PDOException $e){
        echo $e->getMessage();
    }

    if ($query->rowCount()){
        user_session($data);
        return true;
    } else {
        // Je peux utiliser le throw pour me renvoyer une erreur.
        throw new Exception("Cet utilisateur n'existe pas.");
    }
    $db = null; 
}

function logout ()
{
    $_SESSION["user_id"]='';
    if(empty($_SESSION['user_id']))
    {
        $url=BASE_URL.'index.php';
        header("Location: $url");
    }
}

function user_session($id)
{
    $_SESSION["user_id"] = $id;
    return true;
}