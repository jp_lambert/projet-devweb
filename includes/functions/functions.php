<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/includes/functions/validate.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/includes/functions/data/user.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/includes/classes/user.php';

if($_SERVER["REQUEST_METHOD"] == "POST"){
    form_routing();
}

function folder($path){
    return $_SERVER['DOCUMENT_ROOT'] . $path;
}

function form_values($nom){
    echo (isset($_POST[$nom]) ? $_POST[$nom] : "");    
}

function form_routing(){
    if (isset($_POST["contact"])){
        $errors = validate_form($_POST);
    }

    if (isset($_POST["register"])){
        if (count(validate_form($_POST)) < 1){
            $register = register_user($_POST);
            header('Location: profil.php?message=Votre compte à bien été créé');
        }
    }

    if (isset($_POST["login"])){
        if (count(validate_form($_POST)) < 1){
            try {
                $login = login($_POST); 
                return $login;
            } catch(Exception $e){
                echo $e->getMessage();
            }
        }
    }
}

//dump and die
function d($data){
    echo "<pre>";
    var_dump($data);
    echo "</pre>";
}
function dd($data){
    echo "<pre>";
    var_dump($data);
    echo "</pre>";
    die;
}