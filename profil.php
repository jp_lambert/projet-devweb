<?php
include_once $_SERVER["DOCUMENT_ROOT"] . '/includes/functions/functions.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/includes/functions/data/user.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/includes/parts/header.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/includes/search-header.php';

if (isset($_SESSION['uid'])) {
    include_once $_SERVER['DOCUMENT_ROOT'] . '/includes/parts/profil.php';
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . '/includes/parts/login.php';
}

include_once './includes/parts/footer.php';
